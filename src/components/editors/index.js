import ArrayEditor from './array-editor';
import BooleanEditor from './boolean-editor';
import StringEditor from './string-editor';
import RuleEditors from './rules';
import FieldTypeEditor from './field-type-editor';

export default {
    ArrayEditor,
    BooleanEditor,
    StringEditor,
    ...RuleEditors,
    FieldTypeEditor,
};