import DataMap from './data-map';
import VerticalResize from './vertical-resize';

export default {
    DataMap,
    VerticalResize
};