import Common from './common';
import Editors from './editors';

import vueJsonEditor from 'vue-json-editor';

export default {
    ...Common,
    ...Editors,
    vueJsonEditor
};