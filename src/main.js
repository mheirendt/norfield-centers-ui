import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import filters from './core/filters';
import components from './components';
import vuetify from './plugins/vuetify';
import './plugins/vuelayers';
import './plugins/vueresize';
import 'roboto-fontface/css/roboto/roboto-fontface.css';
import '@mdi/font/css/materialdesignicons.css';

for (let filter in filters) {
  Vue.filter(filter, filters[filter]);
}

for (let component in components) {
  Vue.component(component, components[component]);
}

Vue.config.productionTip = false;

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount("#app");
