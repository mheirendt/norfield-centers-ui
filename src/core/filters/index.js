import CamelSpaces from './camel-spaces';
import Capitalize from './capitalize';

export default {
    CamelSpaces,
    Capitalize
};