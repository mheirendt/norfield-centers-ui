import _ from 'lodash';

export default function (value) {
    return _.startCase(value);
}