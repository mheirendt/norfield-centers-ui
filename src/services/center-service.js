import axios from 'axios';
import Center from '@norfield/center';

export default class CenterService {
    async FindOne(filter) {
        const headers = {
            accept: 'application/json',
            'Content-Type': 'application/json'
        };
        const response = await axios.get('http://localhost:3000/centers/julie', {
            headers
        });
        return new Center(response.data);
    }
}