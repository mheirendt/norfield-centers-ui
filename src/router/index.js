import Vue from "vue";
import VueRouter from "vue-router";
import Center from "../views/Center";

Vue.use(VueRouter);

const routes = [
  {
    path: "/center",
    name: "Center",
    component: Center
  },
  {
    path: '*', redirect: '/center'
  }
];

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
});

export default router;
