import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi',
  },
  theme: {
    dark: false,
    themes: {
      dark: {
        primary: '#84C2E1',
        accent: '#B5FFEC',
        secondary: '#B2ECA9',
        info: '#D0E073',
        warning: '#FB8C00',
        error: '#FF5252'
      },
      light: {
        primary: '#122448', // Dark Blue
        secondary: '#00cc95', // Green
        accent: '#f26226', // Orange
        info: '#DBF321',
        warning: '#FB8C00',
        error: '#FF5252'
      }
    },
    options: {
      customProperties: true
    },
  }
});
