# client

## Project setup
```
npm install
```

## Initialize DB
```
npm install -g mongo-seeding-cli
```
```
seed -u 'mongodb://127.0.0.1:27017/norfield' --drop-database ./seed
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
